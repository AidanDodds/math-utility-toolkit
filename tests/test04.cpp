#include "framework.h"
#include "tests.h"

using namespace framework;

void cTest4::init( void )
{
    setTitle( "linear interpolation test" );
    setNumPoints( 2 );
}

void cTest4::tick( void ) 
{
    draw::colour( 0xffffff );
    draw::line( point[0], point[1] );
        
    mut::circle2 c1
    (
        lerp( point[0], point[1], framework::delta ),
        12
    );

    mut::circle2 c2
    (
        lerp( point[0], point[1], mut::smoothstep( framework::delta ) ),
        12
    );

    draw::circle( c1 );
    draw::circle( c2 );

}
