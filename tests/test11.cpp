#include "framework.h"
#include "tests.h"

using namespace framework;

void cTest11::init( void )
{
    setTitle( "edge plane intersect test" );
    setNumPoints( 4 );
}

void cTest11::tick( void ) 
{
    mut::vec2 dif = normalize( point[1] - point[0] );
    mut::plane2 plane( point[0], point[1] );

    // draw plane
    draw::colour( 0x5890d0 );
    draw::line( point[0], point[1] );
    draw::line( point[0], point[0]-dif*640 );
    draw::line( point[1], point[1]+dif*640 );
    mut::vec2 m = (point[0] + point[1]) * .5f;
    draw::colour( 0xff0000 );
    draw::line( m, m + plane.normal * 8.f );

    // edge
    mut::edge2 edge( point[2], point[3] );

    mut::vec2 isec;
    if ( mut::intersect( edge, plane, isec ) )
    {
        draw::colour( 0xffffff );
        draw::circle( mut::circle2( isec, 8 ) );

        draw::colour( 0xff0000 );
    }
    else
        draw::colour( 0xffffff );
    
    // draw edge
    draw::line( edge.a, edge.b );
}
