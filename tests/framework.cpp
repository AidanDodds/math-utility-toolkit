#include <stdlib.h>

#define SDL_MAIN_HANDLED
#include <SDL2\SDL.h>
#pragma comment( lib, "sdl2.lib" )

#include "framework.h"
#include "tests.h"

const int scr_width  = 640;
const int scr_height = 480;

namespace framework
{
    int testNum = 0;
    cTest *test = nullptr;

    int nPoints = 4; // number of points to work with
    int pick    =-1; // index of picked point
    mut::vec2 point[ nMaxPoints ];

    SDL_Window   *window = nullptr;
    SDL_Renderer *render = nullptr;
    
    float delta = 0.f;

    void setTitle( const char *name )
    {
        SDL_SetWindowTitle( window, name );
    }

    void setNumPoints( int num )
    {
        if ( num < 0 )            num = 0;
        if ( num >= nMaxPoints )  num = nMaxPoints;
        nPoints = num;
    }
    
    void draw::plot( const mut::vec2 &a )
    {
        SDL_RenderDrawPoint( render, (int)a.x, (int)a.y );
    }

    void draw::colour( int c )
    {
        SDL_SetRenderDrawColor
            ( render, (c>>16)&0xff, (c>>8)&0xff, (c)&0xff, (c>>24)&0xff );
    }

    void draw::line( const mut::vec2 &a, const mut::vec2 &b )
    {
        SDL_RenderDrawLine( render, (int)a.x, (int)a.y, (int)b.x, (int)b.y );
    }

    void draw::arrow( const mut::vec2 &s, const mut::vec2 &d, float size )
    {
        // arrow shaft
        SDL_RenderDrawLine( render, (int)s.x, (int)s.y, (int)d.x, (int)d.y );
        // arrow head
        mut::vec2 n = mut::normalize( d - s );
        mut::vec2 c = mut::cross( n );
        line( d, d - n*size + c*size*.5f ); 
        line( d, d - n*size - c*size*.5f ); 
    }

    void draw::circle( const mut::circle2 &a )
    {
        float step = mut::pi2 * (1.f / a.r);
        mut::vec2 old = mut::vec2
            ( a.c.x + mut::sin( 0.f ) * a.r, a.c.y + mut::cos( 0.f ) * a.r );
        for ( float i=0.f; i<=mut::pi2*1.01f; i+=step )
        {
            mut::vec2 cur = mut::vec2
                ( a.c.x + mut::sin( i ) * a.r, a.c.y + mut::cos( i ) * a.r );
            line( old, cur );
            old = cur;
        }
    }

    void draw::rect  ( const mut::box2 &a )
    {
        SDL_Rect rect = 
        { 
            (int)a.min.x,
            (int)a.min.y,
            (int)(a.max.x-a.min.x),
            (int)(a.max.y-a.min.y)
        };
        SDL_RenderDrawRect( render, &rect );
    }

    bool init( void )
    {
        SDL_SetMainReady( );
        if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
            return false;

        if ( SDL_CreateWindowAndRenderer
            ( scr_width, scr_height, 0, &window, &render ) != 0 )
            return false;

        for ( int i=0; i<nMaxPoints; i++ )
        {
            float hw = (float)( scr_width  / 2 );
            float hh = (float)( scr_height / 2 );
            point[i] = mut::vec2
            (
                hw + mut::rand::randf() * hw * .5f,
                hh + mut::rand::randf() * hh * .5f
            );
        }

        return true;
    }

    bool pump( void )
    {
        SDL_Event event;
        while ( SDL_PollEvent( &event ) )
        {
            if ( event.type == SDL_QUIT )
                return false;

            if ( event.type == SDL_KEYDOWN )
            {

                // change tests
                if ( event.key.keysym.sym == SDLK_RIGHT )
                {
                    cTest *temp = changeTest( testNum + 1 );
                    if ( temp == nullptr )
                        continue;

                    testNum++;
                    if ( test != nullptr )
                        delete [] test;

                    test = temp;
                    test->init( );
                }

                // change tests
                if ( event.key.keysym.sym == SDLK_LEFT )
                {
                    cTest *temp = changeTest( testNum - 1 );
                    if ( temp == nullptr )
                        continue;

                    testNum--;
                    if ( test != nullptr )
                        delete [] test;

                    test = temp;
                    test->init( );
                }

            }
        }
        return true;
    }

    void shutdown( void )
    {
        SDL_DestroyRenderer( render );
        SDL_DestroyWindow  ( window );

        SDL_Quit( );
    }

    void tickPoints( void )
    {
        int mx, my, mb;
        mb = SDL_GetMouseState( &mx, &my );

        mut::vec2 mousePos = mut::vec2( (float)mx, (float)my );

        bool mousedown = (mb&SDL_BUTTON_LEFT) > 0;
        bool doPick = mousedown && (pick==-1);
        if ( pick == -1 && doPick )
            pick = 0;

        if (! mousedown )
            pick = -1;

        framework::draw::colour( 0x4567b9 );
        for ( int i=0; i<nPoints; i++ )
        {
            mut::circle2 tc( point[i], 7 );
            draw::circle( tc );

            if (! doPick )
                continue;

            if ( mut::distance
                ( mousePos, point[i] ) < mut::distance( mousePos, point[pick] ) )
                pick = i;
        }

        if ( pick>-1 )
            point[pick] = mut::vec2( (float)mx, (float)my );
    }

    void tickDelta( void )
    {
        // update the delta
        delta  = ((float)(SDL_GetTicks( )%5000)) * 0.0002f;
        delta *= 2.f;
        if ( delta > 1.f )
            delta = 1.f - (delta-1.f);
    }

};

int main( int argc, char **args )
{
    atexit( framework::shutdown );

    if (! framework::init( ) )
        return -1;

    // set to the first test
    framework::testNum = 0;
    framework::test = changeTest( framework::testNum );
    framework::test->init( );

    // event pump
    while ( framework::pump( ) )
    {
        // clear the screen buffer
        framework::draw::colour( 0x1f1f1f );
        SDL_RenderClear
            ( framework::render );

        //
        framework::tickDelta( );

        // update the hot points
        framework::tickPoints( );
        
        // update the current test
        if ( framework::test != nullptr )
            framework::test->tick( );

        // update the screen
        SDL_RenderPresent
            ( framework::render );

        SDL_Delay( 1 );
    }

    return 0;
}
