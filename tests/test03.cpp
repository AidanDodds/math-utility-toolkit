#include "framework.h"
#include "tests.h"

using namespace framework;

void cTest3::init( void )
{
    setTitle( "random number generation" );
    setNumPoints( 2 );
}

void cTest3::tick( void ) 
{
    draw::colour( 0xffffff );
    for ( int i=0; i<300; i++ )
    {
        mut::vec2 a = mut::vec2( mut::rand::gaussf( ), mut::rand::gaussf( ) );
        draw::plot( point[0] + a * 64.f );

        mut::vec2 b = mut::vec2( mut::rand::randf( ), mut::rand::randf( ) );
        draw::plot( point[1] + b * 64.f );
    }
}
