#include "framework.h"
#include "tests.h"

using namespace framework;

struct sEntity
{
    mut::circle2 c;
    mut::vec2    a;

    mut::vec2 acm;

    float speed;
};

const int numEntities = 12;
static sEntity entity[ numEntities ];

static
void drawEntity( const sEntity &e )
{
    draw::circle( e.c );
    draw::arrow ( e.c.c, e.c.c + e.a * e.c.r, 8 );
}

static
void tickEntity( sEntity &e )
{
    e.c.c = e.c.c + e.acm;
    e.acm = mut::vec2( 0.f, 0.f );
        
    e.a = mut::clerp( e.a, point[0] - e.c.c, 0.008f );
    e.a = mut::normalize( e.a );

    e.speed  = (e.a * mut::normalize( point[0] - e.c.c )) * 0.1f;
    e.speed *= mut::smoothstep( mut::aproach( e.c.c, point[0], 64.f, 128.f ) );
    e.speed *= 27.f / e.c.r;


    e.c.c = e.c.c + e.a * e.speed;
    draw::colour( 0xB3861E );
    drawEntity( e );
}

void cTest15::init( void )
{
    setTitle( "entity aproach test" );
    setNumPoints( 1 );

    for ( int i=0; i<numEntities; i++ )
    {
        sEntity &e = entity[ i ];
        e.c.c = mut::vec2( 320 + mut::rand::gaussf()*96, 240 + mut::rand::gaussf()*96 );
        e.a   = mut::normalize( mut::vec2( mut::rand::gaussf(), mut::rand::gaussf( ) ) );
        e.c.r = 19.f + mut::rand::randf() * 8;
    }
}

void cTest15::tick( void ) 
{
    for ( int i=0; i<numEntities; i++ )
    {
        for ( int j=i+1; j<numEntities; j++ )
        {
            mut::vec2 res;
            if ( mut::collide( entity[i].c, entity[j].c, res ) )
            {
                entity[i].acm += res * .5f;
                entity[j].acm -= res * .5f;
            }
        }
        tickEntity( entity[i] );
    }
}