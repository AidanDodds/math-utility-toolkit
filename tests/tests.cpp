#include "tests.h"

// 
cTest * changeTest( int num )
{
    switch ( num )
    {
    case (  0 ): return new cTest1;
    case (  1 ): return new cTest2;
    case (  2 ): return new cTest3;
    case (  3 ): return new cTest4;
    case (  4 ): return new cTest5;
    case (  5 ): return new cTest6;
    case (  6 ): return new cTest7;
    case (  7 ): return new cTest8;
    case (  8 ): return new cTest9;
    case (  9 ): return new cTest10;
    case ( 10 ): return new cTest11;
    case ( 11 ): return new cTest12;
    case ( 12 ): return new cTest13;
    case ( 13 ): return new cTest14;
    case ( 14 ): return new cTest15;
    default:
        return nullptr;
    };
}