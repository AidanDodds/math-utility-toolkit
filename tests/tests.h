#pragma once

struct cTest
{
    virtual void init( void ) = 0;
    virtual void tick( void ) = 0;
};

cTest * changeTest( int num );

// ray box intersect test
struct cTest1 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// cubic interpolation test
struct cTest2 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// random number generator tests
struct cTest3 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// linear interpolation test
// ease in ease out test
struct cTest4 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// circular interpolation test
struct cTest5 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// point in triangle
struct cTest6 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// point plane projection
struct cTest7 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// circle edge intersection
struct cTest8 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// point edge projection
struct cTest9 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// point edge projection
struct cTest10 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// plane edge intersection
struct cTest11 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// plane side value test
struct cTest12 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// circumcircle test
struct cTest13 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// circle circle intersection test
struct cTest14 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};

// 
struct cTest15 : public cTest
{
    virtual void init( void );
    virtual void tick( void );
};