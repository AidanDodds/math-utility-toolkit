#pragma once

#include "../source/mut.h"

namespace framework
{
    void setNumPoints( int num );

    const int nMaxPoints = 16;
    extern mut::vec2 point[ nMaxPoints ];

    void setTitle( const char *str );

    namespace draw
    {
        void colour( int c );
        void line  ( const mut::vec2 &a, const mut::vec2 &b );
        void arrow ( const mut::vec2 &a, const mut::vec2 &b, float size );
        void circle( const mut::circle2 &a );
        void rect  ( const mut::box2 &a );
        void plot  ( const mut::vec2 &a );
    };
    
    // timer that oscillates linearly between 1.f and 0.f at a rate of 0.2hz
    extern float delta;

};