#include "framework.h"
#include "tests.h"

using namespace framework;

static mut::vec2 current1;
static mut::vec2 current2;

void cTest9::init( void )
{
    setTitle( "point edge projection" );
    setNumPoints( 3 );
}

void cTest9::tick( void ) 
{

    mut::vec2 dif = normalize( point[1] - point[0] );

    mut::edge2 edge( point[0], point[1] );
    
    draw::colour( 0x5890d0 );
    draw::line( point[0], point[1] );

    mut::vec2 proj = mut::project( edge, point[2] );
    
    draw::colour( 0xffffff );
    draw::arrow( point[2], proj, 8 );
}
