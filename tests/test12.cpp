#include "framework.h"
#include "tests.h"

using namespace framework;

void cTest12::init( void )
{
    setTitle( "plane sidevalue tests" );
    setNumPoints( 3 );
}

void cTest12::tick( void ) 
{
    mut::vec2 dif = normalize( point[1] - point[0] );
    mut::plane2 plane( point[0], point[1] );

    // draw plane
    draw::colour( 0x5890d0 );
    draw::line( point[0], point[1] );
    draw::line( point[0], point[0]-dif*640 );
    draw::line( point[1], point[1]+dif*640 );
    mut::vec2 m = (point[0] + point[1]) * .5f;
    draw::colour( 0xff0000 );
    draw::line( m, m + plane.normal * 8.f );

    mut::s32 sv = mut::test( plane, point[2] );

    if ( sv > 0 )
        draw::colour( 0x00ff00 );

    if ( sv < 0 )
        draw::colour( 0xff0000 );

    if ( sv == 0 )
        draw::colour( 0x0000ff );

    draw::circle( mut::circle2( point[2], 12.f ) );
}
