#include "framework.h"
#include "tests.h"

using namespace framework;

void cTest13::init( void )
{
    setTitle( "circumcircle test" );
    setNumPoints( 3 );
}

void cTest13::tick( void ) 
{
    mut::circle2 circle( point[0], point[1], point[2] );
    draw::circle( circle );
}
