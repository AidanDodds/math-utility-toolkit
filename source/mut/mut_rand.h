/*
 *     _____          __  .__       ____ ___   __  .__.__  .__  __           ___________           .__   __   .__  __   
 *    /     \ _____ _/  |_|  |__   |    |   \_/  |_|__|  | |__|/  |_ ___.__. \__    ___/___   ____ |  | |  | _|__|/  |_ 
 *   /  \ /  \\__  \\   __\  |  \  |    |   /\   __\  |  | |  \   __<   |  |   |    | /  _ \ /  _ \|  | |  |/ /  \   __\
 *  /    Y    \/ __ \|  | |   Y  \ |    |  /  |  | |  |  |_|  ||  |  \___  |   |    |(  <_> |  <_> )  |_|    <|  ||  |  
 *  \____|__  (____  /__| |___|  / |______/   |__| |__|____/__||__|  / ____|   |____| \____/ \____/|____/__|_ \__||__|  
 *          \/     \/          \/                                    \/                                      \/        
 *
 */
#pragma once

#include "mut_1d_types.h"
#include "mut_2d_types.h"

namespace mut
{
namespace rand
{
    extern mut::u32 _useed;
    extern mut::s32 _sseed;

    // info	: provide the random number seed
    static inline
    void _seedRand( u32 seed )
    {
	    _sseed = _useed = seed;
    }

    // info	: generic unsigned random number generator
    // out	: unsigned integer 0 to 0xFFFF
    static inline
    u32 randi( void )
    {
	    _useed *= 1103515245;
	    _useed += 12345;
	    return _useed >> 15;
    }

    // info	: constrained random number
    static inline
    u32 randi( u32 max )
    {
	    return randi() % max;
    }

    // info	: random number range
    static inline
    u32 randi( u32 min, u32 max )
    {
	    return min + (randi() % (max-min));
    }

    // 
    static inline
    float randf( void )
    {
	    float scale = 0.0000305185f;
	    const int c1 = 1103515245;
	    const int c2 = 12345;
	    s32 &s  = _sseed;
	    s32  a  = ( s = s * c1 + c2 ) >> 16;
	    return (float) a * scale;
    }

    // info  : gaussian distribution random number
    // out   : float -1.0f to +1.0f 
    static inline
    float gaussf( void )
    {
	    // scale factor
	    float scale  = 0.125;			//				1.0f / taps		( 8.0 )
	          scale *= 0.0000305185f;	//				1.0f / rand_max	( 0x7FFF )
	          scale *= 1.22474487139f;	// std_deviat..	1.0f / ( sqrt( 8.0f / 12.0f ) )
	    // glibc constants
	    const int c1 = 1103515245;
	    const int c2 = 12345;
	    // 
	    s32 &s  = _sseed;
	    s32  a  = ( s = s * c1 + c2 ) >> 16;
	         a += ( s = s * c1 + c2 ) >> 16;
	         a += ( s = s * c1 + c2 ) >> 16;
	         a += ( s = s * c1 + c2 ) >> 16;
	         a += ( s = s * c1 + c2 ) >> 16;
	         a += ( s = s * c1 + c2 ) >> 16;
	         a += ( s = s * c1 + c2 ) >> 16;
	         a += ( s = s * c1 + c2 ) >> 16;
	    // 
	    return (float) a * scale;
    }

}; // mut::rand
}; // mut