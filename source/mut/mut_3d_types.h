/*
 *     _____          __  .__       ____ ___   __  .__.__  .__  __           ___________           .__   __   .__  __   
 *    /     \ _____ _/  |_|  |__   |    |   \_/  |_|__|  | |__|/  |_ ___.__. \__    ___/___   ____ |  | |  | _|__|/  |_ 
 *   /  \ /  \\__  \\   __\  |  \  |    |   /\   __\  |  | |  \   __<   |  |   |    | /  _ \ /  _ \|  | |  |/ /  \   __\
 *  /    Y    \/ __ \|  | |   Y  \ |    |  /  |  | |  |  |_|  ||  |  \___  |   |    |(  <_> |  <_> )  |_|    <|  ||  |  
 *  \____|__  (____  /__| |___|  / |______/   |__| |__|____/__||__|  / ____|   |____| \____/ \____/|____/__|_ \__||__|  
 *          \/     \/          \/                                    \/                                      \/        
 *
 */
#pragma once

namespace mut
{
    //
    // 3d vector
    //
    struct vec3
    {
        vec3( void )
        {}

        vec3( const vec3 & a )
            : x( a.x ), y( a.y )
        {}

        vec3( float ax, float ay, float az )
            : x(ax), y(ay), z(az)
        {}

	    float x, y, z;
    };

    // 
    // 3d ray
    // 
    struct ray3
    {
        ray3( void )
        {}

        ray3( const vec3 &ap, const vec3 &ad )
            : p( ap ), d( ad )
        {}

	    vec3 p, d;
    };

    // 
    // 3d box
    //
    struct box3
    {
        box3( void )
        {}

        box3( const vec3 &a, const vec3 &b )
            : min( a ), max( b )
        {}

	    vec3 min, max;
    };

    //
    // 3d matrix
    //
    struct mat3
    {
        mat3( void )
        { }

        float e[ 9 ];
    };

    //
    //
    //
    struct plane3
    {
        vec3  normal;
        float d;
    };

    //
    //
    //
    struct sphere3
    {
        vec3  c;
        float r;
    };

};
