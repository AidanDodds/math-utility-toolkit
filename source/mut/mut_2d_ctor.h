/*
 *     _____          __  .__       ____ ___   __  .__.__  .__  __           ___________           .__   __   .__  __   
 *    /     \ _____ _/  |_|  |__   |    |   \_/  |_|__|  | |__|/  |_ ___.__. \__    ___/___   ____ |  | |  | _|__|/  |_ 
 *   /  \ /  \\__  \\   __\  |  \  |    |   /\   __\  |  | |  \   __<   |  |   |    | /  _ \ /  _ \|  | |  |/ /  \   __\
 *  /    Y    \/ __ \|  | |   Y  \ |    |  /  |  | |  |  |_|  ||  |  \___  |   |    |(  <_> |  <_> )  |_|    <|  ||  |  
 *  \____|__  (____  /__| |___|  / |______/   |__| |__|____/__||__|  / ____|   |____| \____/ \____/|____/__|_ \__||__|  
 *          \/     \/          \/                                    \/                                      \/        
 *
 */
#pragma once

#include "mut_2d_types.h"
#include "mut_common.h"

namespace mut
{
    // vector from angle
    inline
    vec2::vec2( float a )
        : x( sin( a ) ), y( cos( a ) )
    {}

    // plane from two points
    inline
    plane2::plane2( const vec2 &a, const vec2 &b )
    {
        // find the vector between 'a' and 'b' 
	    normal = cross( normalize( b - a ) );
	    // find the distance from the origin
	    d = a * normal;
    }

    // circle from three points (circumcircle)
    inline
    circle2::circle2( const vec2 &p1, const vec2 &p2, const vec2 &p3 )
    {
        vec2 _ab = p2 - p1;
        vec2 _cd = p3 - p1;
        // crazyness
        float e = _ab.x * (p1.x + p2.x) + _ab.y * ( p1.y + p2.y );
        float f = _cd.x * (p1.x + p3.x) + _cd.y * ( p1.y + p3.y );
        float g = 2.f * (_ab.x * (p3.y - p2.y) - _ab.y * (p3.x - p2.x));
        // no divide by zero
        if ( g == 0.f )
            return;
        // position
        c.x = (_cd.y * e - _ab.y * f) / g;
        c.y = (_ab.x * f - _cd.x * e) / g;
        // radius
        r = length( p1 - c );
    }

};