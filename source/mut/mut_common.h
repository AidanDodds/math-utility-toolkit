/*
 *     _____          __  .__       ____ ___   __  .__.__  .__  __           ___________           .__   __   .__  __   
 *    /     \ _____ _/  |_|  |__   |    |   \_/  |_|__|  | |__|/  |_ ___.__. \__    ___/___   ____ |  | |  | _|__|/  |_ 
 *   /  \ /  \\__  \\   __\  |  \  |    |   /\   __\  |  | |  \   __<   |  |   |    | /  _ \ /  _ \|  | |  |/ /  \   __\
 *  /    Y    \/ __ \|  | |   Y  \ |    |  /  |  | |  |  |_|  ||  |  \___  |   |    |(  <_> |  <_> )  |_|    <|  ||  |  
 *  \____|__  (____  /__| |___|  / |______/   |__| |__|____/__||__|  / ____|   |____| \____/ \____/|____/__|_ \__||__|  
 *          \/     \/          \/                                    \/                                      \/        
 *
 */
#pragma once

namespace mut
{

    // pi
    static const float pi  = 3.141592653589793f;
    static const float pi2 = 3.141592653589793f * 2.f;

    //
    static const float epsilon = 0.001f;

    //
    // clamp to range
    //
    template < typename T >
    static inline
    T clampv( const T min, const T cur, const T max )
    {
	    if ( cur < min ) return min;
	    if ( cur > max ) return max;
					     return cur;
    }

    //
    // absolute value   
    // 
    template < typename T >
    static inline
    T absv( const T a )
    {
	    if ( a < 0 )
		    return -a;
	    return  a;
    }

    //
    // minimum of dual absolute values
    //
    template < typename T >
    static inline
    T absminv( T a, T b )
    {
	    if ( a < 0 ) a = -a;
	    if ( b < 0 ) b = -b;
	    if ( a < b ) return a;
	    else		 return b;
    }

    //
    // select once of two values with the smallest magnitude
    //
    template < typename T >
    static inline
    T minmagv( T a, T b )
    {
	    T i = a;
	    T j = b;
	    if ( a < 0 ) a = -a;
	    if ( b < 0 ) b = -b;
	    if ( a < b ) return i;
	    else		 return j;
    }

    //
    // minimal value
    //
    template < typename T >
    static inline
    T minv( const T a, const T b )
    {
	    if ( a < b ) return a;
	    else		 return b;
    }

    //
    // maximal value
    //
    template < typename T >
    static inline
    T maxv( const T a, const T b )
    {
	    if ( a > b ) return a;
	    else		 return b;
    }

    //
    // take sign of a value
    //
    template < typename T >
    static inline
    T signv( const T a )
    {
	    if ( a < 0 )
		    return (T)(-1);
		    return (T)( 1);
    }

    //
    // fast reciprical square route
    //
    extern
    float fast_rsqrtf( float in );

    //
    // fast reciprical square route
    //
    static inline
    float fast_sqrtf( float in )
    {
        return 1.f / fast_rsqrtf( in );
    }

    //
    // sin and cosine
    //
    float sin( float a );
    float cos( float a );
    
    // 
    // ease in and ease out curve
    // range 0 -> 1
    // 
    static inline
    float smoothstep( float x )
    {
	    return x*x*(3 - 2*x);
    }

    //
    // cosine like interpolation
    //
    static inline
    float interp_cosine( float a, float b, float x )
    {
	          x -= .5f;
	    float z  = absv<float>( x ); // (x<0.f) ? -x : x;
	          x  = x - x * z + .25f;
	    return a*(1.f-x) + b*x;
    }

    //
    // cubic interpolation
    // 0     1 --- 2     3
    //
    static inline
    float interp_cubic( float v[4], float x )
    {
	    float &a=v[0], &b=v[1], &c=v[2], &d=v[3];

	    float p = (d-c) - (a-b);
	    return p*x*x*x + ((a-b)-p)*x*x + (c-a)*x + b;
    }

};