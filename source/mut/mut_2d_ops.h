/*
 *     _____          __  .__       ____ ___   __  .__.__  .__  __           ___________           .__   __   .__  __   
 *    /     \ _____ _/  |_|  |__   |    |   \_/  |_|__|  | |__|/  |_ ___.__. \__    ___/___   ____ |  | |  | _|__|/  |_ 
 *   /  \ /  \\__  \\   __\  |  \  |    |   /\   __\  |  | |  \   __<   |  |   |    | /  _ \ /  _ \|  | |  |/ /  \   __\
 *  /    Y    \/ __ \|  | |   Y  \ |    |  /  |  | |  |  |_|  ||  |  \___  |   |    |(  <_> |  <_> )  |_|    <|  ||  |  
 *  \____|__  (____  /__| |___|  / |______/   |__| |__|____/__||__|  / ____|   |____| \____/ \____/|____/__|_ \__||__|  
 *          \/     \/          \/                                    \/                                      \/        
 *
 */
#pragma once

#include "mut_2d_types.h"

namespace mut
{
    //
    // vec2
    //

    // scale vector
    static inline
    vec2 operator * ( const vec2& r, const float scale )
    {
        return vec2( r.x * scale, r.y * scale );
    };
    
    // scale vector
    static inline
    vec2 operator * ( const float scale, const vec2& r )
    {
        return vec2( r.x * scale, r.y * scale );
    };

    // scale vector
    static inline
    vec2 operator / ( const vec2& r, const float scale )
    {
        return vec2( r.x / scale, r.y / scale );
    };
    
    // scale vector
    static inline
    vec2 operator / ( const float scale, const vec2& r )
    {
        return vec2( r.x / scale, r.y / scale );
    };
    
    // add vector
    static inline
    vec2 operator + ( const vec2& a, const vec2& b )
    {
        return vec2( a.x + b.x, a.y + b.y );
    }
    
    // subtract vector
    static inline
    vec2 operator - ( const vec2& a, const vec2& b )
    {
        return vec2( a.x - b.x, a.y - b.y );
    }
    
    // dot product
    static inline
    float operator * ( const vec2& a, const vec2& b )
    {
        return a.x*b.x + a.y*b.y;
    }

    // vector matrix multiplication
    static inline
    vec2 operator * ( const vec2& a, const mat2& b )
    {
        return vec2
        (
            a.x * b.e[0] + a.y * b.e[2],
            a.y * b.e[1] - a.x * b.e[3]
        );
    }

};