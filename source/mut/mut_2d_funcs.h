/*
 *     _____          __  .__       ____ ___   __  .__.__  .__  __           ___________           .__   __   .__  __   
 *    /     \ _____ _/  |_|  |__   |    |   \_/  |_|__|  | |__|/  |_ ___.__. \__    ___/___   ____ |  | |  | _|__|/  |_ 
 *   /  \ /  \\__  \\   __\  |  \  |    |   /\   __\  |  | |  \   __<   |  |   |    | /  _ \ /  _ \|  | |  |/ /  \   __\
 *  /    Y    \/ __ \|  | |   Y  \ |    |  /  |  | |  |  |_|  ||  |  \___  |   |    |(  <_> |  <_> )  |_|    <|  ||  |  
 *  \____|__  (____  /__| |___|  / |______/   |__| |__|____/__||__|  / ____|   |____| \____/ \____/|____/__|_ \__||__|  
 *          \/     \/          \/                                    \/                                      \/        
 *
 */
#pragma once

#include "mut_2d_types.h"
#include "mut_2d_ops.h"
#include "mut_common.h"
#include "mut_1d_types.h"

namespace mut
{
    // ray2 box intersection test
    // returns true if intersects and hit stores location
    // return 0 = no hit
    //        1 = x axis
    //        2 = y axis
    int intersect( const ray2 &ray, const box2 &box, vec2 &hit );

    // make a unit vector
    static inline
    vec2 normalize( const vec2 &vec )
    {
        float rlen = fast_rsqrtf( vec.x*vec.x + vec.y*vec.y );
        return vec2( vec.x*rlen, vec.y*rlen );
    };

    // matrix identity
    static inline
    mat2 identity( void )
    {
        return mat2
        ( 
            1.f, 0.f,
            1.f, 0.f
        );
    }

    // create a rotation matrix
    static inline
    mat2 rotate( float angle )
    {
        float s = sin( angle );
        float c = cos( angle );
        return mat2( s, c, c,-s );
    };

    // vector cross product
    static inline
    vec2 cross( const vec2 &a )
    {
        return vec2( a.y,-a.x );
    }

    // vector length
    static inline
    float length( const vec2 &a )
    {
        float rsqrt = fast_rsqrtf( a.x*a.x + a.y*a.y );
        return 1.f / rsqrt;
    }

    // squared vector length
    static inline
    float lengthSqr( const vec2 &a )
    {
        return a.x*a.x + a.y*a.y;
    }

    // distance squared
    static inline
    float distanceSqr( const vec2 &a, const vec2 &b )
    {
        float dx = a.x-b.x;
        float dy = a.y-b.y;
        return dx * dx + dy * dy;
    }

    // linear distance
    static inline
    float distance( const vec2 &a, const vec2 &b )
    {
        return length( a - b );
    }

    // minumum value
    static inline
    vec2 minv( const vec2 &a, const vec2 &b )
    {
        return vec2( minv( a.x, b.x ), minv( a.y, b.y ) );
    }

    // maximum value
    static inline
    vec2 maxv( const vec2 &a, const vec2 &b )
    {
        return vec2( maxv( a.x, b.x ), maxv( a.y, b.y ) );
    }

    // rotate a vector
    static inline
    vec2 rotate( const vec2 &a, float r )
    {
        float s = sin( r );
        float c = cos( r );
        return vec2
            ( a.x*c + a.y*s, 
              a.y*c - a.x*s );
    }

    // linear interpolation
    static inline
    vec2 lerp( const vec2 &a, const vec2 &b, const float i )
    {
        return a + (b-a) * i;
    }

    // circular linear interpolation
    //
    // the damp parameter can be used to have the clerp slow when
    // the current angle is close to the target angle.  It is dependent
    // on all vector lengths however.
    static inline
    vec2 clerp( const vec2 &current, const vec2 &target, float speed, float damp=1.f )
    {

        // rotation coefficient
        float r = clampv<float>( -1.f, ( cross( current ) * target ) * damp, 1.f ) * speed;
        // rotate towards target
        return rotate( current, r );
    }

    // circular non-linear interpolation
    // has an ease out style
    static inline
    vec2 cnlerp( const vec2 &current, const vec2 &target, float speed )
    {
        // vector to target
        vec2 t = normalize( target );
        // current vector
        vec2 i = normalize( current );
        vec2 j = cross( i );
        // rotation coefficient
        float r = (1-t*i) * signv( j * t ) * speed;
        // 
        return rotate( current, r );
    }

    // test if a point 'b' is inside the triangle 'a'
    // return 'true' if inside
    static inline
    bool inside( const vec2 a[ 3 ], vec2 &b )
    {
	    float ap  = a[0].x *    b.y - a[0].y *    b.x;
	    float bp  = a[1].x *    b.y - a[1].y *    b.x;
	    float cp  = a[2].x *    b.y - a[2].y *    b.x;
	    float ab  = a[0].x * a[1].y - a[0].y * a[1].x;
	    float bc  = a[1].x * a[2].y - a[1].y * a[2].x;
	    float ca  = a[2].x * a[0].y - a[2].y * a[0].x;
	    float abc = signv( bc + ca + ab );
	    if ( abc*(bc-bp+cp) <= 0.0f ) return false;
	    if ( abc*(ca-cp+ap) <= 0.0f ) return false;
	    if ( abc*(ab-ap+bp) <= 0.0f ) return false;
        return true;
    }

    // project the vector 'b' onto plane 'a'
    static inline
    vec2 project( const plane2 &a, const vec2 &b )
    {
	    // find the vector of 'b's origin to 'a'
	    vec2 d = b - a.d * a.normal;
	    // find the distance of 'd' from 'a'
	    float dp = a.normal * d;
	    // push the point onto the plane 'a'
	    return b - a.normal * dp;
    }

    // circle edge intersect
    static inline
    bool intersect( const circle2 &c, const edge2 &e, vec2 &i1, vec2 & i2 )
    {
    #define SQR( a ) ((a)*(a))
	    // help make things readable (will get optimised out)
	    const float xA = e.a.x; const float yA = e.a.y;
	    const float xB = e.b.x; const float yB = e.b.y;
	    const float xC = c.c.x; const float yC = c.c.y;
	    const float r  = c.r;
	    // find equation parameters
	    float pa = SQR( xB-xA ) + SQR( yB-yA );
	    float pc = SQR( xA-xC ) + SQR( yA-yC ) - SQR( r );
	    float pb = ( (xB-xA)*(xA-xC) + (yB-yA)*(yA-yC) ) * 2.0f;
	    // find the descriminator
	    float dsc = SQR( pb )-( 4*pa*pc );
	    // there is no intersection
	    if ( dsc <= 0.0f )
		    return false;
	    // intersection parameters
        float sd = 1.f / fast_rsqrtf( dsc );

	    float d1 = (-pb - sd) / ( pa+pa );
	    float d2 = (-pb + sd) / ( pa+pa );
	    // intersection point 1
	    i1.x = xA + d1 * ( xB-xA );
	    i1.y = yA + d1 * ( yB-yA );
	    // intersection point 2
	    i2.x = xA + d2 * ( xB-xA );
	    i2.y = yA + d2 * ( yB-yA );
	    // we found intersections
	    return true;
    #undef SQR
    }

    // find the closest position on 'a' to point 'b' and put the result in 'b'
    // point 'b' is confined to the extent of 'a' only
    static inline
    vec2 project( const edge2 &a, const vec2 &b )
    {
	    vec2 d = a.b - a.a;
	    vec2 c = b - a.a;
	    // dot product divided by squared length of 'd'
	    float t  = (d*c) / (d.x*d.x+d.y*d.y);
        // clip to line endings
	    if ( t < 0.0f ) return a.a;
	    if ( t > 1.0f ) return a.b;
        // project onto line
        return a.a + d * t;
    } 
        
    // clip and edge to the inside of a box
    extern
    bool clip( const box2 &a, const edge2 &b, edge2 &out );

    // find the intersection of 'a' and 'b' and place it in 'c'
    // return 'true' if 'a' and 'b' intersect
    static inline
    bool intersect( const edge2 &a, const plane2 &b, vec2 &c )
    {
	    vec2 ray = a.b - a.a;
	    float r = b.normal * ray;
        if ( r == 0.0f )
            return false;

        float t = (b.d - b.normal * a.a) / r;
        c = a.a + ray * t;

        return ( t >= 0.0f && t <= 1.0f );
    }

    // test the relationship between a and b
    // return  1 when b is on positive side of a
    // return  0 when b lies on a
    // return -1 when b is on negative side of a
    static inline
    s32 test( const plane2 &a, const vec2 &b )
    {
	    // dot product of [x,y] with perp of 'a'
	    float dp  = b * a.normal;
	          dp -= a.d;
	    // are we essentialy coincident
	    if ( absv<float>( dp ) < epsilon )
		    return  0;
	    // are we on the negative side
	    if ( dp < 0.0f )
            return -1;
	        return  1;
    }

    //
    //
    //
    static inline
    bool intersect( const circle2 &a, const circle2 &b, vec2 &i1, vec2 &i2 )
    {
        vec2 d = b.c - a.c;
        float l = length( d );
        if ( l > a.r+b.r )
            return false;
        
        if ( l < absv( a.r - b.r ) )
            return false;

        float i = (a.r*a.r - b.r*b.r + l*l) / (2.f * l);

        vec2 c = a.c + d * (i / l);

        float h = fast_sqrtf( a.r*a.r - i*i );

        vec2 r = cross( d ) * (h / l);

        i1 = c + r;
        i2 = c - r;

        return true;
    }

    // return a scale of membership for the region of space
    // around a target point.  If a point lies outside the outer ring
    // the value is 1.f.  If a point lies inside the inner ring, the value
    // is 0.f.  Half way between the two returns .5f.  Usefull for aproach
    // scaling.
    static inline
    float aproach( const vec2 &cur, const vec2 &dst, float inner, float outer )
    {
        inner *= inner;
        outer *= outer;
        float d = distanceSqr( cur, dst );
        if ( d < inner )
            return 0.f;
        if ( d > outer )
            return 1.f;
        return (d-inner) / (outer-inner);
    }

    // find a collision resolution vector for the overlap of 'a' and 'b'
    // the resolution vector is what 'a' should do to avoid 'b'
    static inline
    bool collide( const circle2 &a, const circle2 &b, vec2 &res )
    {
        vec2 dif = b.c - a.c;
        // squared sum of radius
        float d1 = (a.r+b.r)*(a.r+b.r);
        // squared distance
        float d2 = lengthSqr( dif );
        // not touching so exit
        if ( d2 > d1 )
            return false;
        // find overlap
        float d3 = d1 - d2;
        // scale the difference vector
        res = (dif / d2) * -d3;
        return true;
    }

    // get a unique code specifying the orientation of a this vector
    // 0 --    1 -+    2 +-    3 ++
    static inline
    int quadrantCode( const vec2 &v )
    {
#if 0
        u32 ix  = *(int*)&v.x;
        u32 iy  = *(int*)&v.y;
            ix &= 0x80000000;
            iy &= 0x80000000;
        u32 ic  = (ix>>31) | (iy>>30);
            ic ^= 0x3;
        return ic;

#else
        return (v.x>0.f) + (v.y>0.f) * 2;
#endif
    }

};