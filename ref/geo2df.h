/*
 *	   ______                     __    _ __  
 *	  / ____/___ _____ ___  ___  / /   (_) /_ 
 *	 / / __/ __ `/ __ `__ \/ _ \/ /   / / __ \
 *	/ /_/ / /_/ / / / / / /  __/ /___/ / /_/ /
 *	\____/\__,_/_/ /_/ /_/\___/_____/_/_.___/ 
 *  ..:: Game Creation Utility Library ::..
 *
 * -	by Aidan Dodds (c) (09 / 2013)
 */

#pragma once
#include "types.h"
#include "mathf.h"

// 
struct sVec2f
{
	float x, y;

	explicit sVec2f( float ax, float ay ) : x( ax ), y( ay ) { }
	explicit sVec2f( void ) : x( 0 ), y( 0 ) { }

private:

// if we have disabled copy constructors
#ifdef FFP_NO_COPY
		sVec2f( const sVec2f & );
#endif
};

// 
struct sPlane2f
{
	sVec2f normal;	// plane normal
	float  d;		// distance from the origin
};

//
struct sCircle2f
{
	sVec2f center;	// center
	float  r;		// radius
};

//
struct sRect2f
{
	sVec2f min;
	sVec2f max;
};

//
struct sEdge2f
{
	sVec2f a, b;	// end points a and b
};

//
struct sTriangle2f
{
	sVec2f a, b, c;	// vertices
};

// ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- FLOATING POINT BINARY OPERATORS

// 
#ifndef FFP_NO_COPY

	//
	inline sVec2f operator + ( const sVec2f &a, const sVec2f &b )
	{
		return sVec2f( a.x + b.x, a.y + b.y );
	}

	//
	inline sVec2f operator - ( const sVec2f &a, const sVec2f &b )
	{
		return sVec2f( a.x - b.x, a.y - b.y );
	}

	//
	inline sVec2f operator * ( const sVec2f &a, const float s )
	{
		return sVec2f( a.x * s,  a.y * s );
	}

#endif

// dot product
inline float operator * ( const sVec2f &a, const sVec2f &b )
{
	return a.x * b.x + a.y * b.y;
}

// ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- FLOATING POINT OPERATORS

// scale 
inline void operator *= ( sVec2f &a, const float b )
{
	a.x *= b;		a.y *= b;
}

// addition
inline void operator += ( sVec2f &a, const sVec2f &b )
{
	a.x += b.x;		a.y += b.y;
}

// subtraction
inline void operator -= ( sVec2f &a, const sVec2f &b )
{
	a.x -= b.x;		a.y -= b.y;
}

// ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- FLOATING POINT ARITHMETIC

// find the plane formed with points 'a' and 'b' and place it in 'c'
extern void geo_find( const sVec2f &a, const sVec2f &b, sPlane2f &c );

// find the smallest circle encompasing points 'a' and 'b' and place it in 'c'
extern void geo_find( const sVec2f &a, const sVec2f &b, sCircle2f &c );

// find the smallest rectangle encompasing points 'a' and 'b' and place it in 'c'
extern void geo_find( const sVec2f &a, const sVec2f &b, sRect2f &c );

// find the smallest circle that encompases rectangle 'a' and place it in 'b'
extern void geo_find( const sRect2f &a, sCircle2f &b );

// find the smallest rectangle that encompases circle 'a' and place it in 'b'
extern void geo_find( const sCircle2f &a, sRect2f &b );

// find the plane that falls along the edge 'a' and place it in 'b'
extern void geo_find( const sEdge2f &a, sPlane2f &b );

// turn 'a' into a unit vector
extern void geo_normalise( sVec2f &a );

// find the cross product, which in 2D is just a vector
// with equal magnitude perpendicular to 'a'
inline void geo_cross( sVec2f &a )
{
	float x = a.x;
		a.x = a.y;
		a.y =-x;
}

// turn 'a' into an aproximate unit vector (fast function)
inline void get_fastNorm( sVec2f &a )
{
	float sqrSum  = a.x * a.x + a.y * a.y;
	float invMag  = fastSqrtf( sqrSum );
	        a.x  *= invMag;
	        a.y  *= invMag;
}

// find the magniture of 'a'
// return a float with the magnitude
extern float geo_magnitude( const sVec2f &a );

// find the aproximate inverse magnitude of 'a' (fast function)
inline float get_fastInvMag( const sVec2f &a )
{
	float  sqrSum = (a.x * a.x) + (a.y * a.y);
	return fastSqrtf( sqrSum );
}

// find the dot product of 'a' and 'b'
// return a float with the dot product
inline float geo_dot( const sVec2f &a, const sVec2f &b )
{
	return (a.x * b.x) + (a.y * b.y);
}

// place the crossproduct of 'a' into 'b'
inline void geo_cross( const sVec2f &a, sVec2f &b )
{
	b.x = a.y;
	b.y =-a.x;
}

// reflect point 'c' around the normal 'a' at position 'b'
extern void geo_reflect( const sVec2f &a, const sVec2f &b, sVec2f &c );

// reflect edge 'b' against plane 'a' at its intersection point
// this can be visualised like folding the edge 'b' where it meets plane 'a'
extern bool geo_reflect( const sPlane2f &a, sEdge2f &b );

// test if a point 'b' is inside the triangle 'a'
// return 'true' if inside
extern bool geo_test( const sTriangle2f &a, const sVec2f &b );

// find the so called 'side value' of plane 'a' and point 'b'
// return +1 - positive side
// return  0 - coincident
// return -1 - neagtive side
extern sint32 geo_test( const sPlane2f &a, const sVec2f &b );

// find which edge ray 'b'->'c' intersects in 'a'
// return  0 - no intersection
// return  1 - left edge
// return  2 - right edge
// return  3 - top edge
// return  4 - bottom edge
extern sint32 geo_test( const sRect2f &a, const sVec2f &b, const sVec2f &c );

// test if circles 'a' and 'b' intersect
// return 'true' if there is intersection
extern bool geo_test( const sCircle2f &a, const sCircle2f &b );

// test the relationship between plane 'a' and edge 'b'
// return  1 - the edge is entirely on the positive side of 'a'
// return  0 - the edge straddles plane 'a'
// return -1 - the edge is entirely on the negative side of 'a'
extern sint32 geo_test( const sPlane2f &a, const sEdge2f &b );

// test if these edges overlap at some point
// return  'true' if these edges overlap
extern bool geo_test( const sEdge2f &a, const sEdge2f &b );

// project the point 'b' onto plane 'a'
extern void geo_project( const sPlane2f &a, sVec2f &b );

// project point 'b' onto edge 'a' and put the result in 'b', the
// result is confined to only lie on the edge 'a'
extern void geo_project( const sEdge2f &a, sVec2f &b );

// project 'b' into the circle 'a'
extern void geo_project( const sCircle2f &a, sVec2f &b );

// project the circle 'b' onto the possitive side of plane 'a'
// such that its edge lies on the plane 'a'
extern void geo_project( const sPlane2f &a, sCircle2f &b );

// find the intersection of 'a' and 'b' and place it in 'c'
inline bool geo_intersect( const sPlane2f &a, const sPlane2f &b, sVec2f &c )
{
	// viia Cramers Rule
	float a1 = a.normal.x;	float a2 = b.normal.x;
	float b1 = a.normal.y;	float b2 = b.normal.y;
	float d1 = a.d;			float d2 = b.d;
	// find the determinant
	float dt = a1 * b2 - a2 * b1;
	// check for paralel lines
	if ( fastAbsf( dt ) < 0.00001f )
		return false;
	// find the intersection
	c.x = ((b2 * d1) - (b1 * d2)) / dt;
	c.y = ((a1 * d2) - (a2 * d1)) / dt;
	// intersection found
	return true;
}

// find the intersection points of edge 'b' and circle 'a' treating
// edge 'b' as if it forms a 2D hyper plane. 'c' and 'd' receive
// the intersection points
// return 'true' if 'a' and 'b' intersect
extern bool   geo_intersect( const sCircle2f &a, const sEdge2f &b, sVec2f &c, sVec2f &d );

// find the intersection of 'a' and 'b' and place it in 'c' and 'd'
// return 'true' if 'a' and 'b' intersect
extern bool   geo_intersect( const sCircle2f &a, const sPlane2f &b, sVec2f &c, sVec2f &d );

// find the intersection of 'a' and 'b' and place it in 'c' and 'd'
// return 'true' if 'a' and 'b' intersect
extern bool   geo_intersect( const sCircle2f &a, const sCircle2f &b, sVec2f &c, sVec2f &d );

// find the intersection of 'a' and 'b' and place it in 'c' and 'd'
// return 'true' if 'a' and 'b' intersect
extern bool   geo_intersect( const sPlane2f &a, const sRect2f &b, sVec2f &c, sVec2f &d );

// find the intersection of 'a' and 'b' and place the result in 'c'
// return 'true' if 'a' and 'b' intersect
extern bool   geo_intersect( const sEdge2f &a, const sEdge2f &b, sVec2f &c );

// find the intersection of 'a' and 'b' and place it in 'c'
// return 'true' if 'a' and 'b' intersect
extern bool   geo_intersect( const sEdge2f &a, const sPlane2f &b, sVec2f &c );

// find the distance between points 'a' and 'b'
// returns the distance
extern float  geo_distance( const sVec2f &a, const sVec2f &b );

// find the aproximate distance between points 'a' and 'b' (fast function)
// returns the distance
inline float  geo_fastDist( const sVec2f &a, const sVec2f &b )
{
	float x = b.x - a.x;
	float y = b.y - a.y;
	return fastSqrtf( x * x + y * y );
}

// return the squared distance between points 'a' and 'b'
inline float  geo_SqrDist( const sVec2f &a, const sVec2f &b )
{
	float x = b.x - a.x;
	float y = b.y - a.y;
	return x * x + y * y;
}

// find the distance between an edge of circle 'a' and point 'b'
// returns 0.0f anywhere inside the circle
extern float  geo_distance  ( const sCircle2f &a, const sVec2f &b );

// find the distance between the edges of circle 'a' and circle 'b'
extern float  geo_distance  ( const sCircle2f &a, const sCircle2f &b );

// find the distance between point 'b' and the closest point on plane 'a'
extern float  geo_distance  ( const sPlane2f &a, const sVec2f &b );

// find the distance between point 'b' and line segment 'a'
extern float  geo_distance  ( const sEdge2f &a, const sVec2f & b );

// use Gauss theorum to find the area of a set of vertices.
// the main application of this would be for pressure soft body
// simulations. I cant imagine many other uses at the moment.
extern float  geo_gaussArea ( const sVec2f *b, const uint32 points );

// clip the edge 'b' to the inside of rectangle 'a'
extern bool   geo_clip      ( const sRect2f &a, sEdge2f &b );

// clip edge 'b' by an array of planes 'a' of quantity 'planes'
extern bool   geo_clip      ( const sPlane2f &a, const uint32 planes, sEdge2f &b );

// clip edge 'b' to the positive edge of plane 'a'
// return -  1 fully on positive side
// return -  0 spanning so the edge has been clipped
// return - -1 the edge is fully on the negative side
extern sint32 geo_clip		( const sPlane2f &a, sEdge2f &b );

// split edge 'b' by plane 'a' and place the two halves in 'pos' and 'neg'
// pos receives the edge that lies on the positive side of 'a'
// neg receives the edge that lies on the negative side of 'a'
// return -  1 fully on positive side of 'a', only 'pos' filled
// return -  0 spans plane 'a', 'pos' and 'neg' are filled
// return - -1 full on negative side of 'a', onle 'neg' filled
extern sint32 geo_split     ( const sPlane2f &a, const sEdge2f &b, sEdge2f &pos, sEdge2f &neg );