/*
 *	   ______                     __    _ __  
 *	  / ____/___ _____ ___  ___  / /   (_) /_ 
 *	 / / __/ __ `/ __ `__ \/ _ \/ /   / / __ \
 *	/ /_/ / /_/ / / / / / /  __/ /___/ / /_/ /
 *	\____/\__,_/_/ /_/ /_/\___/_____/_/_.___/ 
 *  ..:: Game Creation Utility Library ::..
 *
 * -	by Aidan Dodds (c) (09 / 2013)
 */

#pragma once
#include "types.h"

// 
#define PI 3.141592653589793f

// round down to nearest whole number
inline float ffloorf( float x )
{
	return (float) ( (int) x );
}

// round up to nearest whole number
inline float fceilf( float x )
{
	return (float) ( (int) (x+1.0f) );
}

// floating point modulus function
inline float modf( float a, float b )
{
	//
	if ( b == 0.0f )
		return a;
	//
	float z = a / b;
	float m = a - b * ffloorf( z );
	// wrap negative numbers
	if ( m < 0 ) m += b;
	//
	return m;
}

// fast sinus aproximation
// will wrap into range
// period range = -1 to +1
// output range = -1 to +1
inline float fastSinf( float a )
{
	// wrap into range
	a = modf( a + 1.0f, 2.0f ) - 1.0f;
	// z = abs( a )
	float z = a; if ( z < 0.0f ) z = -z;
	return (4.0f * a - 4.0f * a * z );
}

#ifdef FASTCOSF_ALT_VER
inline float fastCosf( float a )
{
	// wrap into range
	a = modf( a + 0.5f, 2.0f ) - 0.5f;
	float z = a - 0.5f;
	// x = abs( z )
	float x = z; if ( x < 0 ) x = -x;
	float r = ( 4 * z * x ) / ( 1 - ( 4 * z ) );
	return r;
}
#else
// fast cosine aproximation
// will wrap into range
// period range = -1 to +1
// output range = -1 to +1
inline float fastCosf( float a )
{
	// offset by 90 degrees
	a += 0.5f;
	// wrap into range
	a = modf( a + 1.0f, 2.0f ) - 1.0f;
	// z = abs( a )
	float z = a; if ( z < 0.0f ) z = -z;
	return (4.0f * a - 4.0f * a * z );
}
#endif

// fast reciprical square root aproximation
inline float fastSqrtf( float number )
{
        long  i;
        float x2, y;
        const float threehalfs = 1.5f;
        x2 = number * 0.5f;
        y  = number;
        i  = * ( long * ) &y;
        i  = 0x5f37642f - ( i >> 1 );
        y  = * ( float * ) &i;
        y  = y * ( threehalfs - ( x2 * y * y ) );
        return y;
}

// fast reciprical square root aproximation
// with a higher accuracy
inline float fastSqrtf2( float number )
{
        long  i;
        float x2, y;
        const float threehalfs = 1.5f;
        x2 = number * 0.5f;
        y  = number;
        i  = * ( long * ) &y;
        i  = 0x5f375a86 - ( i >> 1 );
        y  = * ( float * ) &i;
        y  = y * ( threehalfs - ( x2 * y * y ) );
        y  = y * ( threehalfs - ( x2 * y * y ) );
        return y;
}

//
inline float fastAbsf( float in )
{
	uint32 *a  = (uint32*)&in;
	       *a &= 0x7FFFFFFF;
	return *(float*)a;
}

//
inline sint32 signf( float a )
{
	// treat as integer
	uint32 *b = (uint32*)&a;
	// shift sign bit to 0-or-2 then subract from 1
	return 1-(((*b) >> 30) & 0x2);
}

// ease in and ease out shape
// range 0 -> 1
inline float smoothf( float x )
{
	return x*x*(3 - 2*x);
}

/*
// reference
inline double rootf( const double fg )
{
	double n = fg / 2.0;
	double lstX = 0.0;

	while( n != lstX )	//xxx: use tolerance
	{
		lstX = n;
		n = (n + fg/n) / 2.0;
	}
	return n;
}
*/