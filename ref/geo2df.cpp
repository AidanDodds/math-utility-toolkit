/*
 *	   ______                     __    _ __  
 *	  / ____/___ _____ ___  ___  / /   (_) /_ 
 *	 / / __/ __ `/ __ `__ \/ _ \/ /   / / __ \
 *	/ /_/ / /_/ / / / / / /  __/ /___/ / /_/ /
 *	\____/\__,_/_/ /_/ /_/\___/_____/_/_.___/ 
 *  ..:: Game Creation Utility Library ::..
 *
 * -	by Aidan Dodds (c) (09 / 2013)
 */

#include "geo2Df.h"
#include "mathf.h"

// standard math library
#include <math.h>

// square helper macro
#define SQR( A ) ( (A)*(A) )
#define EPSILON 0.1f

// CS line clip codes
const int INSIDE	= 0;
const int LEFT		= 1;
const int RIGHT		= 2;
const int BOTTOM	= 4;
const int TOP		= 8;

//
static inline void geo_copy( const sVec2f &a, sVec2f &b )
{
	b.x   = a.x;
	b.y   = a.y;
}

static inline void geo_copy( const sEdge2f &a, sEdge2f &b )
{
	b.a.x = a.a.x;
	b.a.y = a.a.y;
	b.b.x = a.b.x;
	b.b.y = a.b.y;
}

// find the plane formed with points 'a' and 'b' and place it in 'c'
void geo_find( const sVec2f &a, const sVec2f &b, sPlane2f &c )
{
	// find the vector between 'a' and 'b' 
	sVec2f n( b.x - a.x, b.y - a.y );
	// normalise it
	geo_normalise( n );
	// store the perpendicular normal
	c.normal.x = n.y;
	c.normal.y =-n.x;
	// find the distance from the origin
	c.d = geo_dot( a, c.normal );
}

// find the smallest circle encompasing points 'a' and 'b' and place it in 'c'
void geo_find( const sVec2f &a, const sVec2f &b, sCircle2f &c )
{
	// take the mid point of 'a' and 'b' as the center
	c.center.x = ( b.x + a.x ) * 0.5f;
	c.center.y = ( b.y + a.y ) * 0.5f;
	// take half the distance between 'a' and 'b' for the radius
	c.r = sqrtf
	(
		SQR( b.x - a.x ) +
		SQR( b.y - a.y )
	);
	c.r *= 0.5f;
}

// find the smallest rectangle encompasing points 'a' and 'b' and place it in 'c'
void geo_find( const sVec2f &a, const sVec2f &b, sRect2f &c )
{
}

// find the smallest circle that encompases rectangle 'a' and place it in 'b'
void geo_find( const sRect2f &a, const sCircle2f &b )
{
}

// find the smallest rectangle that encompases circle 'a' and place it in 'b'
void geo_find( const sCircle2f &a, sRect2f &b )
{
	b.min.x = a.center.x - a.r;
	b.min.y = a.center.y - a.r;
	b.max.x = a.center.x + a.r;
	b.max.y = a.center.y + a.r;
}

// find the plane that falls along the edge 'a' and place it in 'b'
void geo_find( const sEdge2f &a, sPlane2f &b )
{
	geo_find( a.a, a.b, b );
}

// turn 'a' into a unit vector
void geo_normalise( sVec2f &a )
{
	float sqrSum  = a.x * a.x + a.y * a.y;
	float invMag  = 1.0f / sqrtf( sqrSum );
	        a.x  *= invMag;
	        a.y  *= invMag;
}

// find the magniture of 'a'
// return a float with the magnitude
float geo_magnitude( const sVec2f &a )
{
	float sqrSum = a.x * a.x + a.y * a.y;
	return sqrtf( sqrSum );
}

// find the so called 'side value' of plane 'a' and point 'b'
// return +1 - positive side
// return  0 - coincident
// return -1 - neagtive side
sint32 geo_test( const sPlane2f &a, const sVec2f &b )
{
	// dot product of [x,y] with perp of 'A'
	float dp  = (b.x * a.normal.x) + (b.y * a.normal.y);
	      dp -= a.d;
	// are we essentialy coincident
	if ( fastAbsf( dp ) < EPSILON )
					 return  0;
	// are we on the negative side
	if ( dp < 0.0f ) return -1;
	else			 return  1;
}

// test if a point 'b' is inside the triangle 'a'
// return 'true' if inside
bool geo_test( sTriangle2f &a, sVec2f &b )
{
	float ap  = a.a.x * b.y   - a.a.y * b.x  ;
	float bp  = a.b.x * b.y   - a.b.y * b.x  ;
	float cp  = a.c.x * b.y   - a.c.y * b.x  ;
	float ab  = a.a.x * a.b.y - a.a.y * a.b.x;
	float bc  = a.b.x * a.c.y - a.b.y * a.c.x;
	float ca  = a.c.x * a.a.y - a.c.y * a.a.x;
	float abc = (float)signf( bc + ca + ab );
	if ( abc*(bc-bp+cp) <= 0.0f ) return false;
	if ( abc*(ca-cp+ap) <= 0.0f ) return false;
	if ( abc*(ab-ap+bp) <= 0.0f ) return false;
								  return true ;
}

//
static inline sint32 orientation( sEdge2f &a, sVec2f &b )
{
	// Linear determinant of the 3 points.
	// This function returns the orientation of px,py on line x1,y1,x2,y2.
	// Look from x2,y2 to the direction of x1,y1.
	// If px,py is on the right, function returns +1
	// If px,py is on the left, function returns -1
	// If px,py is directly ahead or behind, function returns 0
	return signf
		(
			(a.b.x - a.a.x) * (b.y   - a.a.y) -
			(b.x   - a.a.x) * (a.b.y - a.a.y)
		);
}

// test if these edges overlap at some point
// return  'true' if these edges overlap
bool geo_test( sEdge2f &a, sEdge2f &b )
{
	return 	( orientation( a, b.a ) != 
			  orientation( a, b.b )) &&
			( orientation( b, a.a ) !=
			  orientation( b, a.b ));
}

// find which edge ray 'b'.'c' intersects in 'a'
// return  0 - no intersection
// return  1 - left edge
// return  2 - right edge
// return  3 - top edge
// return  4 - bottom edge
sint32 geo_test( sRect2f *a, sVec2f *b, sVec2f *c )
{
	return 0;
}

// test if circles 'a' and 'b' intersect
// return true if there is intersection
bool geo_test( const sCircle2f &a, sCircle2f &b )
{
	// find the diference
	float x = b.center.x - a.center.x;
	float y = b.center.y - a.center.y;
	// use squared distance
	float d  = SQR( x ) + SQR( y );
		  d -= SQR( a.r );
		  d -= SQR( b.r );
	// 
	if ( d <= 0.0f )	return false;
	else				return true;
}

// test the relationship between plane 'b' and edge 'a'
// return  1 - the edge is entirely on the positive side of 'b'
// return  0 - the edge straddles plane 'b'
// return -1 - the edge is entirely on the negative side of 'b'
sint32 geo_test( const sPlane2f &a, sEdge2f &b )
{
	sint32 sv1 = geo_test( a, b.a );
	sint32 sv2 = geo_test( a, b.b );
	sint32 dv  = sv1 + sv2;
	//
	if ( dv >= 1 )	return  1;
	if ( dv <=-1 )	return -1;
					return  0;
}

// project the vector 'b' onto plane 'a'
void geo_project( const sPlane2f &a, sVec2f &b )
{
	// find the vector of 'b's origin to 'a'
	sVec2f d
	(
		b.x - (a.d * a.normal.x),
		b.y - (a.d * a.normal.y)
	);
	// find the distance of 'd' from 'a'
	float dp = geo_dot( a.normal, d );
	// push the point onto the plane 'a'
	b.x -= a.normal.x * dp;
	b.y -= a.normal.y * dp;
}

// project 'b' into the circle 'a'
void geo_project( const sCircle2f &a, sVec2f &b )
{
	// find normal point at 'b' from center of 'a'
	sVec2f d
	(
		a.center.x - b.x,
		a.center.y - b.y
	);
	geo_normalise( d );
	// extend such that it lies on the radius of 'd'
	b.x = a.center.x - (d.x * a.r);
	b.y = a.center.y - (d.y * a.r);
}

// project the circle 'b' onto the positive side of plane 'a'
void geo_project( const sPlane2f &a, sCircle2f &b )
{
	// find the vector of 'b's origin to 'a'
	sVec2f d
	(
		b.center.x - (a.d * a.normal.x),
		b.center.y - (a.d * a.normal.y)
	);
	// find the distance of 'd' from 'a'
	float dp  = geo_dot( a.normal, d );
	// offset by the circle radius
		  dp -= b.r;
	// push the point onto the plane 'a'
	b.center.x -= a.normal.x * dp;
	b.center.y -= a.normal.y * dp;
}

// reflect vector 'c' around the normal 'a' at position 'b'
void geo_reflect( const sVec2f &a, const sVec2f &b, sVec2f &c )
{
	// offset to the origin
	sVec2f v( c.x - b.x, c.y - b.y );
	// find the perpendicular dot product
	float  d = (v.x*a.y) - (v.y*a.x); //geo_dot( &n, &v );
	// reflect using the perpendular normal
	c.x = b.x + v.x - ( 2.0f * a.y * d);
	c.y = b.y + v.y + ( 2.0f * a.x * d);
}

// reflect edge 'b' against plane 'a' at its intersection point
// this can be visualised like folding the edge 'b' where it meets plane 'a'
bool geo_reflect( const sPlane2f &a, sEdge2f &b )
{
	sPlane2f p;
	sVec2f   i;
	// we need a straddle of edge 'b' on plane 'a'
	if ( geo_test( a, b ) != 0 )
		return false;
	// find the intersection point
	geo_find     ( b, p );
	geo_intersect( p, a, i );
	//
	b.a.x = i.x;
	b.a.y = i.y;
	// find the vector of 'b's origin to 'a'
	sVec2f d
	(
		b.b.x - (a.d * a.normal.x),
		b.b.y - (a.d * a.normal.y)
	);
	// find the distance of 'd' from 'a'
	float dp = geo_dot( a.normal, d );
	// push the point onto the plane 'a'
	b.b.x -= a.normal.x * (dp * 2);
	b.b.y -= a.normal.y * (dp * 2);
	//
	return true;
}

// find the intersection points of edge 'e' and circle 'c' treating
// edge 'e' as if it forms a 2D hyper plane. 'a' and 'b' receive
// the intersection points
bool geo_intersect( const sCircle2f &c, const sEdge2f &e, sVec2f &i1, sVec2f &i2 )
{
	// help make things readable (will get optimised out)
	const float xA = e.a.x;      const float yA = e.a.y;
	const float xB = e.b.x;      const float yB = e.b.y;
	const float xC = c.center.x; const float yC = c.center.y;
	const float r  = c.r;
	// find equation parameters
	float pa = SQR( xB-xA ) + SQR( yB-yA );
	float pc = SQR( xA-xC ) + SQR( yA-yC ) - SQR( r );
	float pb = ( (xB-xA)*(xA-xC) + (yB-yA)*(yA-yC) ) * 2.0f;
	// find the descriminator
	float dsc = SQR( pb )-( 4*pa*pc );
	// there is no intersection
	if ( dsc <= 0.0f )
		return false;
	// intersection parameters
	float d1 = (-pb - sqrt( dsc )) / ( pa+pa );
	float d2 = (-pb + sqrt( dsc )) / ( pa+pa );
	// intersection point 1
	i1.x = xA + d1 * ( xB-xA );
	i1.y = yA + d1 * ( yB-yA );
	// intersection point 2
	i2.x = xA + d2 * ( xB-xA );
	i2.y = yA + d2 * ( yB-yA );
	// we found intersections
	return true;
}

// find the intersection of 'a' and 'b' and place it in 'c' and 'd'
// return 'true' if 'a' and 'b' intersect
bool geo_intersect( const sCircle2f *a, const sPlane2f *b, sVec2f *c, sVec2f *d )
{
	return false;
}

// find the intersection of 'a' and 'b' and place it in 'c' and 'd'
// return 'true' if 'a' and 'b' intersect
bool geo_intersect( const sCircle2f *a, const sCircle2f *b, sVec2f *c, sVec2f *d )
{
	return false;
}

// find the intersection of 'a' and 'b' and place it in 'c' and 'd'
// return 'true' if 'a' and 'b' intersect
bool geo_intersect( const sPlane2f *a, const sRect2f *b, sVec2f *c, sVec2f *d )
{
	return false;
}

// find the intersection of 'a' and 'b' and place the result in 'c'
// return 'true' if 'a' and 'b' intersect
bool geo_intersect ( const sEdge2f *a, const sEdge2f *b, sVec2f *c )
{
	return false;
}

// find the intersection of 'a' and 'b' and place it in 'c'
// return 'true' if 'a' and 'b' intersect
bool geo_intersect( const sEdge2f &a, const sPlane2f &b, sVec2f &c )
{
	// 
	sVec2f ray
	(
		a.b.x - a.a.x,
		a.b.y - a.a.y
	);
	// 
	float r = geo_dot( b.normal, ray );
    if ( r == 0.0f )
        return false;
    // 
    float t = (b.d - geo_dot( b.normal, a.a )) / r;
	// 
	c.x = a.a.x + ray.x * t;
	c.y = a.a.y + ray.y * t;
	// 
    return ( t >= 0.0f && t <= 1.0f );
}

// find the distance between points 'a' and 'b'
// returns the distance
float geo_distance( const sVec2f &a, const sVec2f &b )
{
	// find the diference
	float x = b.x - a.x;
	float y = b.y - a.y;
	// 
	return sqrtf( SQR(x) + SQR(y) );
}

// find the distance between an edge of circle 'a' and point 'b'
// returns 0.0f anywhere inside the circle
float geo_distance( const sCircle2f &a, const sVec2f &b )
{
	// find the diference
	float x = b.x - a.center.x;
	float y = b.y - a.center.y;
	// 
	float d  = sqrtf( SQR(x) + SQR(y) );
		  d -= a.r;
	// 
	if ( d < 0.0f )	return 0.0f;
	else			return d;
}

// find the distance between the edges of circle 'a' and circle 'b'
float geo_distance( const sCircle2f &a, const sCircle2f &b )
{
	// find the diference
	float x = b.center.x - a.center.x;
	float y = b.center.y - a.center.y;
	// 
	float d  = sqrtf( SQR(x) + SQR(y) );
		  d -= a.r;
		  d -= b.r;
	// 
	if ( d < 0.0f )	return 0.0f;
	else			return d;
}

// find the distance between point 'b' and the closest point on plane 'a'
float geo_distance( const sPlane2f &a, const sVec2f &b )
{
	// offset b by the plane origin
	float dx = b.x - a.normal.x * a.d;
	float dy = b.y - a.normal.y * a.d;
	// find the dot product of the plane and the normal
	return (dx * a.normal.x) + (dy * a.normal.y);
}

// find the distance between point 'b' and the closest point on line segment 'a'
float geo_distance( const sEdge2f &a, const sVec2f &b )
{
	sVec2f p( b.x, b.y );
	       geo_project ( a, p );
	return geo_distance( b, p );
}

//
static inline float gauss( sVec2f *b, uint32 i, uint32 j )
{
	float x1 = b[i].x;
	float y1 = b[i].y;
	float x2 = b[j].x;
	float y2 = b[j].y;
	// find the magnitude of line 'i' to 'j'
	float mg = fastSqrtf( SQR( x2-x1 ) + SQR( y2+y1 ) );
	// x normal of the line
	float nx =-(y2-y1) * mg;
	// 
	float r12d = sqrtf
	(
		SQR( x1-x2 ) +
		SQR( y1-y2 )
	);
	//
	return 0.5f * fabs( x1-x2 ) * fabs( nx ) * r12d;
}

// use Gauss theorum to find the area of a set of vertices.
// the main application of this would be for pressure soft body
// simulations. I cant imagine many other uses at the moment.
extern float geo_gaussArea ( sVec2f *b, uint32 points )
{
	float volume = 0.0f;
	// work our way around the pressure body
	for ( uint32 i=0; i<points-1; i++ )
		// 
		volume += gauss( b, i, i+1 );
	// close the pressure body
	volume += gauss( b, points-1, 0 );
	// return the volume
	return volume;
}

// find the closest position on 'a' to point 'b' and put the result in 'b'
// point 'b' is confined to the extent of 'a' only
void geo_project( const sEdge2f &a, sVec2f &b )
{	
	sVec2f d
	(
		a.b.x - a.a.x,
		a.b.y - a.a.y
	);
	sVec2f c
	(
		b.x - a.a.x,
		b.y - a.a.y
	);
	// dot product divided by squared length of 'd'
	float t  = geo_dot( d, c );
		  t /= SQR( d.x ) + SQR( d.y );
	//
	if ( t < 0.0f )
	{
		b.x = a.a.x;
		b.y = a.a.y;
	}
	else
	{
		if ( t > 1.0f )
		{
			b.x = a.b.x;
			b.y = a.b.y;
		}
		else
		{
			b.x = a.a.x + t * d.x;
			b.y = a.a.y + t * d.y;
		}
	}
}
 
// out code computation for the CS line clip algorythm
static uint32 outCode( sRect2f &a, float x, float y )
{
	uint32 code = INSIDE;
	if		(x < a.min.x) code |= LEFT;
	else if	(x > a.max.x) code |= RIGHT;
	if		(y < a.min.y) code |= BOTTOM;
	else if	(y > a.max.y) code |= TOP;
	return code;
}

// clip the edge 'b' to the inside of rectangle 'a'
bool geo_clip( sRect2f &a, sEdge2f &b )
{
	// 
	float x0 = b.a.x; float y0 = b.a.y;
	float x1 = b.b.x; float y1 = b.b.y;
	// 
    uint32 outcode0 = outCode( a, x0, y0 );
    uint32 outcode1 = outCode( a, x1, y1 );
    bool accept = false;
	// 
    while ( true )
    {
        if    (!(outcode0 | outcode1))
		{
			accept = true;
			break;
		}
		else if (outcode0 & outcode1)
		{
			break;
		}
		else
		{
			float x, y;
			uint32 outcodeOut = outcode0 ? outcode0 : outcode1;
			//
            if      (outcodeOut & TOP   )
			{
				x = x0 + (x1 - x0) * (a.max.y - y0) / (y1 - y0);
				y = a.max.y;
			}
			else if (outcodeOut & BOTTOM)
			{
				x = x0 + (x1 - x0) * (a.min.y - y0) / (y1 - y0);
				y = a.min.y;
			}
			else if (outcodeOut & RIGHT )
			{
				y = y0 + (y1 - y0) * (a.max.x - x0) / (x1 - x0);
				x = a.max.x;
            }
			else if (outcodeOut & LEFT  )
			{
				y = y0 + (y1 - y0) * (a.min.x - x0) / (x1 - x0);
				x = a.min.x;
			}
			// 
			if (outcodeOut == outcode0)
			{
				x0       = x;
				y0       = y;
				outcode0 = outCode( a, x0, y0 );
            }
			else
			{
                x1       = x;
                y1       = y;
                outcode1 = outCode( a, x1, y1 );
            }
        }
    }
	if ( accept )
	{
		b.a.x = x0; b.a.y = y0;
		b.b.x = x1; b.b.y = y1;
		return true;
    }
	return false;
}

// clip edge 'b' to the positive edge of plane 'a'
// return -  1 fully on positive side
// return -  0 spanning so the edge has been clipped
// return - -1 the edge is fully on the negative side
sint32 geo_clip( sPlane2f &a, sEdge2f &b )
{
	// test each vertex
	sint32 sv1 = geo_test( a, b.a );
	sint32 sv2 = geo_test( a, b.b );
	sint32 dv  = sv1 + sv2;
	// can we shortcut from clipping
	if ( dv >=  1 ) return  1;
	if ( dv <= -1 ) return -1;
	// find the intersection point
	sVec2f point;
	geo_intersect( b, a, point );
	// decide where to place the intersection point
	// to form a clipped edge
	if ( sv1 == 1 )	geo_copy( point, b.b );
	else			geo_copy( point, b.a );
	// return split code
	return 0;
}

// clip edge 'b' by an array of planes 'a' of quantity 'planes'
bool geo_clip( sPlane2f *a, uint32 n, sEdge2f &b )
{
	// cycle through all of the edges
	while ( n-- > 0 )
	{
		// test each vertex
		sint32 sv1 = geo_test( a[n], b.a );
		sint32 sv2 = geo_test( a[n], b.b );
		sint32 dv  = sv1 + sv2;
		// can we short cut out of clipping
		if ( dv >=  1 ) continue;
		if ( dv <= -1 ) return false;
		// find the intersection
		sVec2f point;
		geo_intersect( b, a[n], point );
		// decide where to place the intersection point
		// to form a shortened (clipped) edge
		if ( sv1 == 1 )	geo_copy( point, b.b );
		else			geo_copy( point, b.a );
	}
	//
	return true;
}

// split edge 'b' by plane 'a' and place the two halves in 'pos' and 'neg'
// pos receives the edge that lies on the positive side of 'a'
// neg receives the edge that lies on the negative side of 'a'
// return -  1 fully on positive side of 'a', only 'pos' filled
// return -  0 spans plane 'a', 'pos' and 'neg' are filled
// return - -1 full on negative side of 'a', onle 'neg' filled
sint32 geo_split( sPlane2f &a, sEdge2f &b, sEdge2f &pos, sEdge2f &neg )
{
	// test each vertex on the plane
	sint32 sv1 = geo_test( a, b.a );
	sint32 sv2 = geo_test( a, b.b );
	sint32 dv  = sv1 + sv2;
	//
	if ( dv >=  1 )
	{
		geo_copy( b, pos );
		// entirely positive
		return  1;
	}
	if ( dv <= -1 )
	{
		geo_copy( b, neg );
		// entirely negative
		return -1;
	}
	// find the mid point
	sVec2f point;
	geo_intersect( b, a, point );
	//
	if ( sv1 == 1 )
	{
		// [b.a]  . [point]	: positive
		// [point] . [b.b]		: negative
		geo_copy( b.a  , pos.a );
		geo_copy( point, pos.b );
		geo_copy( point, neg.a );
		geo_copy( b.b  , neg.b );
	}
	else
	{
		// [b.b]  . [point]	: positive
		// [point] . [b.a]		: negative
		geo_copy( point, pos.a );
		geo_copy( b.b  , pos.b );
		geo_copy( b.a  , neg.a );
		geo_copy( point, neg.b );
	}
	// splitting
	return 0;
}